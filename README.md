# Gitlab CI Library

Common library components used in EduIT Gitlab CI.

## Usage

To access a template in `templates`: use the template dir name with tag added, and set the `stage` variable (or more variables if necessary) to the stage the template should be used in.

```
include:
  - component: "$CI_SERVER_FQDN/eduit/gitlab-ci-lib/kaniko-build-std@master"
    inputs:
      stage: doit

stages:
  - doit
```

The definition of input variables like `inputs.stage` is optional, if you use the default values; see below.

### Common Variables
All templates define

- `inputs.tags`: the tag(s) a runner must support to pick up the jobs. Defaults to `- k8s-runner` (is an array!)

## Docker Build
Template *kaniko-build-std* creates a Docker image. The image will be saved to the Gitlab Docker registry.

Every commit launches the build. Such a build is saved as *main* (or *master*, depending on the name of the default branch), as *latest* and with an unique hash identifier.

If a tag is created, an image with this tag will be created. Before tagging, give the build process time to complete, as the tagging job will pull this image, tag it, and push it back.

### Defaults
- `inputs.stage`: build
- `inputs.dockerfile`: Dockerfile
- `inputs.tag_suffix`: "" (appended to all Docker tags)

## ko Build
The template *ko-build-std* builds go images without a Dockerfile. The images are minimal and very secure. For that to happen the application needs to be pure go.

Tagging is planed for future versions.

## Python Linting: pre-commit
The template *python-pre-commit-check* uses a pre-commit configuration to lint Python code. A pre-commit environment can be installed during development with

```bash
pip install pre-commit
pre-commit install

# Try a run
pre-commit run --all-files
```

The pre-commit configuration file will be used by the template to lint the code. Linting failures will cause the pipeline to fail.

### Defaults
- `inputs.stage`: lint
- `inputs.python_version`: "3" (string! See [Python Docker images](https://hub.docker.com/_/python) for valid values)

## Python Packaging
*python-build-wheel* builds a Python package. The wheel archive will be storated as artefact (`dist/<pgk_name>.whl`) and kept for 1 week. To store it permanently, add a release job.

The build process will read a requirements file with `pip install -r requirements.txt`.

### Defaults
- `inputs.stage`: lint
- `inputs.python_version`: "3"

Uses `pip install -r requirements.txt`.

## Python Packaging for pyproject.toml
*python-build-wheel-pytoml* builds a Python package. The wheel archive will be storated as artefact (`dist/<pgk_name>.whl`) and kept for 1 week. To store it permanently, add a release job.

The build process will setup the build environment with `pip install ".[build]"`.

### Defaults
- `inputs.stage`: lint
- `inputs.python_version`: "3"

Uses `pip install -r requirements.txt`.

## Python Release
With *python-release*, a package created by *python-build-wheel* can be release to the Gitlab package library.

### Defaults
- `inputs.stage`: release
